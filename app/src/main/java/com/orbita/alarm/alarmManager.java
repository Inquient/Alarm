package com.orbita.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Админ on 30.06.2016.
 */
public class alarmManager extends WakefulBroadcastReceiver {

    final public static String ONE_TIME="onetime";
    Context context;

    int freq;

    @Override
    public void onReceive(Context context, Intent intent){
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        //Включение экрана. Работает не на всех устройствах.
        PowerManager.WakeLock wl = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wl.acquire();

//        Bundle extras = intent.getExtras();
//        StringBuilder msgStr = new StringBuilder();
//
//        if(extras!=null && extras.getBoolean(ONE_TIME, Boolean.FALSE)){
//            msgStr.append("Одноразовый будильник: ");
//        }

//        Format formatter = new SimpleDateFormat("hh:mm:ss a");
//        msgStr.append(formatter.format(new Date()));
//
//        Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();

        SharedPreferences sPref = context.getSharedPreferences("kind", Context.MODE_PRIVATE);
        alarmStorage storage = new alarmStorage(sPref);
        boolean k = storage.loadKind();
        Log.d("k", String.valueOf(k));

        Intent i = new Intent();
        if(k) {
            i.setClassName("com.orbita.alarm","com.orbita.alarm.alarmSound2");
        }
        if(!k) {
            i.setClassName("com.orbita.alarm", "com.orbita.alarm.alarmSound");
        }
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);

        //wl.release();
    }

    public void setFreq(int freq){
        this.freq = freq;
    }

    public void SetAlarm(Context context, Calendar c)
    {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        this.context = context;
        Intent intent = new Intent(context, alarmManager.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 5 seconds

        am.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), this.freq*1000*60, pi);
//        am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pi);
    }

    public void CancelAlarm(Context context)
    {
        Intent intent = new Intent(context, alarmManager.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
