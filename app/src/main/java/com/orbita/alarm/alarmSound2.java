package com.orbita.alarm;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class alarmSound2 extends AppCompatActivity {

    final Context context = this;

    private MediaPlayer player;
    private Vibrator vibrator;
    private alarmManager alarm;
    ProgressBar p;

    ImageView[] images;
    String[] dishes;
    Set<Integer> menu;
    Iterator<Integer> itr;
    ImageView currFood;
    TextView order;
    final Timer timer = new Timer(true);
    float volume = 0f;

    private void startFadeIn(){
        final int FADE_DURATION = 40000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if(volume>=1f){
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
    }

    private void fadeInStep(float deltaVolume){
        player.setVolume(volume, volume);
        volume += deltaVolume;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            timer.cancel();
            timer.purge();
            if(volume>=0f) {
                volume = volume - 0.1f;
                if(volume<0){
                    volume = 0;
                }
                player.setVolume(volume, volume);
            }
            return true;
        }else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            timer.cancel();
            timer.purge();
            if(volume>=0f) {
                volume = volume + 0.1f;
                if(volume>1){
                    volume = 1;
                }
                player.setVolume(volume, volume);
            }
            return true;
        }else
            return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
//        this.getWindow().addFlags(
//                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
//                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
//                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_alarm_sound2);
        alarm = new alarmManager();

        interfaceSetup();

        play(this, getAlarmSound());

        if(isOnline(this)){
            AD_setup();
        }
    }

    protected void AD_setup(){
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9323060416361289~3356483292");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

    private void interfaceSetup() {
        p = (ProgressBar) findViewById(R.id.progressBar);

        final ImageView img1 = (ImageView) findViewById(R.id.food1);
        final ImageView img2 = (ImageView) findViewById(R.id.food2);
        final ImageView img3 = (ImageView) findViewById(R.id.food3);
        final ImageView img4 = (ImageView) findViewById(R.id.food4);
        final ImageView img5 = (ImageView) findViewById(R.id.food5);
        img1.setTag(getResources().getString(R.string.Coffee));
        img2.setTag(getResources().getString(R.string.Muffin));
        img3.setTag(getResources().getString(R.string.Cake));
        img4.setTag(getResources().getString(R.string.Burger));
        img5.setTag(getResources().getString(R.string.Cookie));
        images = new ImageView[]{img1, img2, img3, img4, img5};
        dishes = new String[]{getResources().getString(R.string.Coffee), getResources().getString(R.string.Muffin),
                getResources().getString(R.string.Cake), getResources().getString(R.string.Burger),
                getResources().getString(R.string.Cookie)};

        menu = setMenu();
        order = (TextView) findViewById(R.id.order);
        itr = menu.iterator();
        currFood = images[itr.next()];
        String s = getResources().getString(R.string.Want) + " " + String.valueOf(currFood.getTag());
        order.setText(s);

        ImageView ord = (ImageView) findViewById(R.id.imageView6);
//        ord.setBackground(currFood.getDrawable());
        ord.setImageDrawable(currFood.getDrawable());
        ord.jumpDrawablesToCurrentState();

        myDragEventListener myDrag = new myDragEventListener();
        ImageView target = (ImageView) findViewById(R.id.imageView4);
        target.setOnDragListener(myDrag);

        for (ImageView img : images) {
            img.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN) {
                        ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
                        ClipData dragData = new ClipData((CharSequence) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                        View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                        v.startDrag(dragData,  // the data to be dragged
                                myShadow,  // the drag shadow builder
                                v,      // no need to use local data - Вот из-за этого пидараса я чуть не разбил монитор
                                0          // flags (not currently used, set to 0)
                        );
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });

        }
        Button a = (Button) findViewById(R.id.button5);
        String d = getResources().getString(R.string.Delay);
        a.setTextSize(20);
        a.setText(d);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/EURO-B.TTF");
        a.setTypeface(custom_font);
    }

    private Set<Integer> setMenu(){
        Set<Integer> generated = new LinkedHashSet<Integer>();
        Random r = new Random();
        while (generated.size() < 4) {
            generated.add(r.nextInt(5));
        }
        return generated;
    }

    private void play(Context context, Uri alert) {
        player = new MediaPlayer();
        long[] mills = {3000L, 500L, 3000L, 500L, 3000L, 500L};
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        try {
            player.setDataSource(context, alert);
            final AudioManager audio = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audio.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                player.setAudioStreamType(AudioManager.STREAM_ALARM);
                player.prepare();
                player.setVolume(volume, volume);
                player.setLooping(true);
                player.start();
                startFadeIn();
                vibrator.vibrate(mills, 2);
            }
        } catch (IOException e) {
            Log.e("Error....", "Check code...");
        }
    }

    private Uri getAlarmSound() {
        SharedPreferences sPref = context.getSharedPreferences("music", Context.MODE_PRIVATE);
        alarmStorage storage = new alarmStorage(sPref);
        String s = storage.loadMusic();
        Uri alertSound = Uri.parse(s);
        Log.d("k", alertSound.getEncodedPath());
        if((alertSound.getEncodedPath().equals("Default")) || (alertSound.getEncodedPath().equals("По умолчанию"))) {
            alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            if (alertSound == null) {
                alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (alertSound == null) {
                    alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                }
            }
        }
        return alertSound;
    }

    public void stop_player(View view) {
        player.stop();
        vibrator.cancel();
        finish();
    }

    public void cancelRepeatingTimer() {
        Context context = this.getApplicationContext();
        if (alarm != null) {
            alarm.CancelAlarm(context);
            Log.d("Error", "Alarm deleted!");
        } else {
            Log.d("Error", "Alarm is not deleted!");
        }
    }

    public void settings2(View view){
        Intent i = new Intent();
        i.setClassName("com.orbita.alarm", "com.orbita.alarm.settings");
        startActivity(i);
    }

    @Override
    public void onBackPressed() {}

    @Override
    public void onDestroy(){
        super.onDestroy();
        vibrator.cancel();
        player.stop();
    }

    protected class myDragEventListener implements View.OnDragListener {

        public boolean onDrag(View v, DragEvent event) {

            final int action = event.getAction();


            switch (action) {
                case DragEvent.ACTION_DROP:
                    View dragView = (View) event.getLocalState();
                    if(dragView.getTag() == currFood.getTag()){
                        if(itr.hasNext()) {
                            currFood = images[itr.next()];
                            String s = getResources().getString(R.string.Want) + " " + String.valueOf(currFood.getTag());
                            order.setText(s);
                            ImageView ord = (ImageView) findViewById(R.id.imageView6);
                            ord.setImageDrawable(currFood.getDrawable());
                            ord.jumpDrawablesToCurrentState();
                        }

                        p.incrementProgressBy(25);
                        dragView.setVisibility(View.INVISIBLE);
                    }
                    else{
                        final RelativeLayout foodLay = (RelativeLayout) findViewById(R.id.foodLay);
                        foodLay.setBackgroundColor(Color.RED);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                foodLay.setBackgroundResource(R.drawable.back);
                            }
                        }, 300);
                    }

                    if(p.getProgress() == 100) {
                        player.stop();
                        vibrator.cancel();
                        cancelRepeatingTimer();
                        Intent i = new Intent();
                        i.setClassName("com.orbita.alarm", "com.orbita.alarm.MainActivity");
                        i.putExtra("fromAlarm", true);
                        startActivity(i);
                    }
                    break;

                // An unknown action type was received.
                default:
                    Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                    break;
            }

            return true;
        }
    };
}