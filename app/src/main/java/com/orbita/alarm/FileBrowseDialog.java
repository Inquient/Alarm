package com.orbita.alarm;

/**
 * Created by Админ on 25.01.2017.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileBrowseDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private View form = null;

    public String musComposition;

    Button buttonUp;
    TextView textFolder;
    ListView dialog_ListView;

    File root;
    File curFolder;
    private List<String> fileList = new ArrayList<String>();

    public interface MyDialogListener{
        public void onDialogListItemClick(DialogFragment dialog, String mComp);
    }

    MyDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            mListener = (MyDialogListener) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);
        form= getActivity().getLayoutInflater()
                .inflate(R.layout.dialoglayout, null);

        textFolder = (TextView) form.findViewById(R.id.folder);
        buttonUp = (Button) form.findViewById(R.id.up);
        buttonUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListDir(curFolder.getParentFile());
            }
        });

        dialog_ListView = (ListView) form.findViewById(R.id.dialoglist);

        root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        curFolder = root;

        ListDir(curFolder);

        dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File selected = new File(fileList.get(position));
                if(selected.isDirectory()) {
                    ListDir(selected);
                } else {
                    String s = selected.toString().substring(selected.toString().lastIndexOf("/")+1,
                            selected.toString().length());
                    TextView t1 = (TextView) getActivity().findViewById(R.id.melody_holder);
                    t1.setText(s);
                    musComposition = selected.toString();
                    mListener.onDialogListItemClick(FileBrowseDialog.this, musComposition);
                    getDialog().dismiss();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return(builder.setTitle(getResources().getString(R.string.Dialog)).setView(form).create());
    }

    @Override
    public void onClick(DialogInterface dialog, int which){

    }

    void ListDir(File f) {
        if(f.equals(root)) {
            buttonUp.setEnabled(false);
        } else {
            buttonUp.setEnabled(true);
        }

        List<String> displayableList = new ArrayList<String>();

        curFolder = f;
        textFolder.setText(f.getPath());

        File[] files = f.listFiles();
        fileList.clear();

        for(File file : files) {
            fileList.add(file.getPath());
            displayableList.add(file.getName());
        }

        ArrayAdapter<String> directoryList = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, displayableList);
        dialog_ListView.setAdapter(directoryList);
    }

    @Override
    public void onDismiss(DialogInterface unused) {
        super.onDismiss(unused);
    }
    @Override
    public void onCancel(DialogInterface unused) {
        super.onCancel(unused);
    }
}
