package com.orbita.alarm;

/**
 * Created by Админ on 27.06.2016.
 */
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class alarmStorage {

    final String ALARM_STORAGE = "alarm";
    final String KIND_STORAGE = "kind";
    final String EXAMPLE_STORAGE = "example";
    final String MUSIC_STORAGE = "music";

    private SharedPreferences sPref = null;
    private SharedPreferences.Editor editor = null;

    alarmStorage(SharedPreferences sPref){
        this.sPref = sPref;
        this.editor = this.sPref.edit();
    }

    public void setPreferences(SharedPreferences sPref){
        this.sPref = sPref;
        this.editor = this.sPref.edit();
    }

    public void alarmSave(List<alarmItem> alarmItemList){
        String s = "";
        for(int i = 0; i < alarmItemList.size(); i++) {
            s += alarmItemList.get(i).getInfo();
        }
        String ss = s.substring(0,s.length()-1);
        this.editor.putString(ALARM_STORAGE, ss);
        this.editor.apply();
    }

    public List<alarmItem> alarmParse(String[] arr){
        List<alarmItem> alarmItemList = new ArrayList<alarmItem>();

        String[] all = this.sPref.getString(ALARM_STORAGE, "alarm").split("\\|");

        String time;
        String week;
        String on;
        String kind;
        String freq;
        String musComposition;

        for(int i = 0; i<all.length; i++){
            time = all[i].substring(0,5);
            on = all[i].substring(5,6);
            kind = all[i].substring(6,7);
            freq = all[i].substring(7,8);
            week = all[i].substring(8,15);
            musComposition = all[i].substring(15);

            alarmItem alarm = new alarmItem();
            alarm.setVisibleDays(arr);
            alarm.setTime(Integer.parseInt(time.substring(0,2)), Integer.parseInt(time.substring(3)));
            if(on.equals("1")){
                alarm.setOn(true);
            }
            if(on.equals("0")){
                alarm.setOn(false);
            }
            if(kind.equals("1")){
                alarm.setKind(true);
            }
            if(kind.equals("0")){
                alarm.setKind(false);
            }
            alarm.setFreq(Integer.parseInt(freq));
            alarm.weekReading(week);
            alarm.setMusComposition(musComposition);

            alarmItemList.add(alarm);
        }

        return alarmItemList;

    }

    public String alarmLoad(){
        return  this.sPref.getString(ALARM_STORAGE, "alarm");
    }

    public void alarmClearing(){
        this.editor.clear();
        this.editor.putString(ALARM_STORAGE, "alarm");
        this.editor.apply();
    }

    public void kindSave(boolean kind){
        this.editor.putBoolean(KIND_STORAGE, kind);
        this.editor.apply();
    }

    public boolean loadKind(){
        return this.sPref.getBoolean(KIND_STORAGE, true);
    }

    public void exampleSave(int example){
        this.editor.putInt(EXAMPLE_STORAGE, example);
        this.editor.apply();
    }

    public int loadExample(){
        return this.sPref.getInt(EXAMPLE_STORAGE, 1);
    }

    public void musicSave(String music){
        this.editor.putString(MUSIC_STORAGE, music);
        this.editor.apply();
    }

    public String loadMusic(){
        return this.sPref.getString(MUSIC_STORAGE, "Default");
    }
}
