package com.orbita.alarm;

import java.util.Calendar;

/**
 * Created by Админ on 07.06.2016.
 */
public class alarmItem {

    String[] days = new String[]{"пн.","вт.","ср.","чт.","пт.","сб.","вс."};
    private String weekDays;

    private boolean[] weekCheck;
    int hour;
    int minute;
    int freq;
    Boolean on;
    Boolean kind;
    String musComposition;

    alarmItem(){
        this.weekDays = "";
        this.weekCheck = new boolean[]{false,false,false,false,false,false,false};
        this.hour = 0;
        this.minute = 0;
        this.freq = 0;
        this.on = true;
        this.kind = true;
        this.musComposition = "Default";
    }

    public void setVisibleDays(String[] d){
        this.days = d;
    }

    //Методы для записи и чтения массива дней недели в файл и из файла
    private String weekWriting(){

        String week = "";
        for(int i = 0; i<7; i++){
            if(this.weekCheck[i]){
                week += "1";
            }
            else{
                week += "0";
            }
        }
        return week;
    }

    public void weekReading(String week){
        char[] c = week.toCharArray();
        for(int i = 0; i<7; i++){
            if(c[i] == '1'){
                this.weekCheck[i] = true;
                weekDays += days[i];
            }
            else{
                this.weekCheck[i] = false;
            }
        }
    }

    //Setters

    public void setWeekCheck(boolean[] b){
        this.weekCheck = b;
        for(int i = 0; i<7; i++){
            if(weekCheck[i]){
                weekDays += days[i];
            }
        }
    }

    public void setTime(int hour, int minute){
        this.hour = hour;
        this.minute = minute;
    }

    public void setOn(boolean on){
        this.on = on;
    }

    public void setFreq(int freq){
        this.freq = 5*freq;
    }

    public void setKind(boolean kind){
        this.kind = kind;
    }

    public void setMusComposition(String musComposition){
        this.musComposition = musComposition;
    }

    //Getters

    public String getWeekDays(){
        String s = this.weekDays;
        return s;
    }

    public boolean[] getWeekCheck(){
        return this.weekCheck;
    }

    public int getNextWeekDay(){
        Calendar current = Calendar.getInstance();
        int dayofweek = 0;

        int d = current.get(Calendar.DAY_OF_WEEK);

        if(d == 1){
            d = 8;
        }

        if(dayofweek == 0) {
            if (weekCheck[d - 2]) {
                if (this.hour > current.get(Calendar.HOUR_OF_DAY)) {
                    dayofweek = d;
                }
                if (this.hour == current.get(Calendar.HOUR_OF_DAY)) {
                    if (this.minute > current.get(Calendar.MINUTE)) {
                        dayofweek = d;
                    }
                }
            }
        }

        if(dayofweek == 0) {
            for (int i = current.get(Calendar.DAY_OF_WEEK)+1; i <= 7; i++) {
                if ((i == 1) && (weekCheck[6])) {
                    dayofweek = i;
                    break;
                }
                if (i != 1) {
                    if (weekCheck[i - 2]) {
                        dayofweek = i;
                        break;
                    }
                }
            }
        }

        if(dayofweek == 0) {
            for (int i = 1; i < current.get(Calendar.DAY_OF_WEEK); i++) {
                if ((i == 1) && (weekCheck[6])) {
                    dayofweek = i;
                    break;
                }
                if (i != 1) {
                    if (weekCheck[i - 2]) {
                        dayofweek = i;
                        break;
                    }
                }
            }
        }

        if(dayofweek == 0) {
            if(weekCheck[d - 2]){
                if(this.hour < current.get(Calendar.HOUR_OF_DAY)){
                    dayofweek = d;
                }
                if(this.hour == current.get(Calendar.HOUR_OF_DAY)){
                    if(this.minute <= current.get(Calendar.MINUTE)){
                        dayofweek = d;
                    }
                }
            }
        }

        if ((dayofweek == 8) && (weekCheck[6])) {
            dayofweek = 1;
        }
        return dayofweek;
    }

    public String getTime(){
        String s1 = String.valueOf(hour);
        String s2 = String.valueOf(minute);
        if (hour <= 9){
            s1 = "0" + s1;
        }
        if (minute <= 9){
            s2 = "0" + s2;
        }
        String all = s1 + ":" + s2;
        return all;
    }

    public Boolean getKind(){
        return this.kind;
    }

    public Boolean getOn(){
        return this.on;
    }

    public int getFreq(){
        return this.freq/5;
    }

    public String getMusComposition(){
        return this.musComposition;
    }

    public String getInfo(){
        int ind_on;
        int ind_kind;
        if(this.on){
            ind_on = 1;
        }
        else{
            ind_on = 0;
        }
        if(this.kind){
            ind_kind = 1;
        }
        else{
            ind_kind = 0;
        }
        String info = getTime() + ind_on + ind_kind + getFreq() + weekWriting() + getMusComposition() + "|";
        return info;
    }
}
