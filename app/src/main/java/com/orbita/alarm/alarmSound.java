package com.orbita.alarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Админ on 06.07.2016.
 */
public class alarmSound extends Activity{

    final Context context = this;

    private MediaPlayer player;
    private Vibrator vibrator;
    private alarmManager alarm;

    final Random random = new Random();

    EditText andswer_field;
    private int andswer;
    private int difficulty;
    final Timer timer = new Timer(true);
    float volume = 0f;

    private void startFadeIn(){
        final int FADE_DURATION = 40000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if(volume>=1f){
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
    }

    private void fadeInStep(float deltaVolume){
        player.setVolume(volume, volume);
        volume += deltaVolume;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            timer.cancel();
            timer.purge();
            if(volume>=0f) {
                volume = volume - 0.1f;
                if(volume<0){
                    volume = 0;
                }
                player.setVolume(volume, volume);
            }
            return true;
        }else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            timer.cancel();
            timer.purge();
            if(volume>=0f) {
                volume = volume + 0.1f;
                if(volume>1){
                    volume = 1;
                }
                player.setVolume(volume, volume);
            }
            return true;
        }else
            return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.alarm_layout);
        alarm = new alarmManager();

        interfaceSetup();

        play(this, getAlarmSound());

        if(isOnline(this)){
            AD_setup();
        }
    }

    protected void AD_setup(){
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9323060416361289~3356483292");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

    public void ready_press(View view){
        int f = 0;
        if(andswer_field.getText().toString().equals("")) {
            alert();
        }
        else{
            f = Integer.parseInt(andswer_field.getText().toString());
        }
        if(f == andswer){
            player.stop();
            vibrator.cancel();
            cancelRepeatingTimer();
            Intent i = new Intent();
            i.setClassName("com.orbita.alarm", "com.orbita.alarm.MainActivity");
            i.putExtra("fromAlarm", true);
            startActivity(i);
        }
        else{
            alert();
        }
    }

    protected void alert(){
        final RelativeLayout all_batya = (RelativeLayout) findViewById(R.id.all_batya);
        all_batya.setBackgroundColor(Color.RED);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                all_batya.setBackgroundResource(R.drawable.back);
            }
        }, 300);
    }

    protected void interfaceSetup(){
        andswer_field = (EditText) findViewById(R.id.andswer_field);
        StringBuilder msgStr = new StringBuilder();
        Format formatter = new SimpleDateFormat("HH:mm");
        msgStr.append(formatter.format(new Date()));

        TextView t1 = (TextView) findViewById(R.id.textView17);
        t1.setText(msgStr);

        SharedPreferences sPref = context.getSharedPreferences("example", Context.MODE_PRIVATE);
        alarmStorage storage = new alarmStorage(sPref);
        difficulty = storage.loadExample();

        int first;
        int second;

        TextView expression = (TextView) findViewById(R.id.expression);
        String s = "";
        if(difficulty == 2){
            boolean b = random.nextBoolean();
            if(b){
                difficulty = 0;
            }
            else{
                difficulty = 1;
            }
        }
        if(difficulty == 0){
            first = random.nextInt(50)+1;
            second = random.nextInt(50)+1;
            this.andswer = first + second;
            s = String.valueOf(first)+"+"+String.valueOf(second)+"=";
        }
        if(difficulty == 1) {
            first = random.nextInt(10)+1;
            second = random.nextInt(10)+1;
            this.andswer = first * second;
            s = String.valueOf(first)+"x"+String.valueOf(second)+"=";
        }
        if(difficulty == 3) {
            int a1 = random.nextInt(10)+1;
            int a2 = random.nextInt(10)+1;
            first = a1*a2;
            int b1 = random.nextInt(a1)+1;
            int b2 = random.nextInt(a2)+1;
            second = b1*b2;
            this.andswer = first-second;
            s = String.valueOf(a1)+"x"+String.valueOf(a2)+"-"+String.valueOf(b1)+"x"+String.valueOf(b2)+"=";
        }
        expression.setText(s);

        Button b = (Button) findViewById(R.id.button10);
        String t = getResources().getString(R.string.Ready);
        b.setTextSize(20);
        b.setText(t);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/EURO-B.TTF");
        b.setTypeface(custom_font);

        Button a = (Button) findViewById(R.id.button);
        String d = getResources().getString(R.string.Delay);
        a.setTextSize(15);
        a.setText(d);
        a.setTypeface(custom_font);
    }

    private void play(Context context, Uri alert) {
        player = new MediaPlayer();
        long[] mills = {3000L, 500L, 3000L, 500L, 3000L, 500L};
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        try {
            player.setDataSource(context, alert);
            final AudioManager audio = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audio.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                player.setAudioStreamType(AudioManager.STREAM_ALARM);
                player.prepare();
                player.setVolume(volume, volume);
                player.setLooping(true);
                player.start();
                startFadeIn();
                vibrator.vibrate(mills, 2);
            }
        } catch (IOException e) {
            Log.e("Error....","Check code...");
        }
    }

    private Uri getAlarmSound() {
        SharedPreferences sPref = context.getSharedPreferences("music", Context.MODE_PRIVATE);
        alarmStorage storage = new alarmStorage(sPref);
        String s = storage.loadMusic();
        Uri alertSound = Uri.parse(s);
        Log.d("k", alertSound.getEncodedPath());
        if((alertSound.getEncodedPath().equals("Default")) || (alertSound.getEncodedPath().equals("По умолчанию"))) {
            alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            if (alertSound == null) {
                alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (alertSound == null) {
                    alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                }
            }
        }
        return alertSound;
    }

    public void cancelRepeatingTimer(){
        Context context = this.getApplicationContext();
        if(alarm != null){
            alarm.CancelAlarm(context);
            Log.d("Error", "Alarm deleted!");
        }else{
            Log.d("Error", "Alarm is not deleted!");
        }
    }

    public void b_1_press(View view){
        andswer_field.append("1");
    }

    public void b_2_press(View view){
        andswer_field.append("2");
    }

    public void b_3_press(View view){
        andswer_field.append("3");
    }

    public void b_4_press(View view){
        andswer_field.append("4");
    }

    public void b_5_press(View view){
        andswer_field.append("5");
    }

    public void b_6_press(View view){
        andswer_field.append("6");
    }

    public void b_7_press(View view){
        andswer_field.append("7");
    }

    public void b_8_press(View view){
        andswer_field.append("8");
    }

    public void b_9_press(View view){
        andswer_field.append("9");
    }

    public void b_0_press(View view){
        andswer_field.append("0");
    }

    public void backspace_press(View view){
        String s = andswer_field.getText().toString();
        if(s.length()>0) {
            andswer_field.setText(s.substring(0, s.length() - 1));
        }
    }

    public void refresh_press(View view){

        int first;
        int second;
        TextView expression = (TextView) findViewById(R.id.expression);
        String s = "";
        if(difficulty == 2){
            boolean b = random.nextBoolean();
            if(b){
                difficulty = 0;
            }
            else{
                difficulty = 1;
            }
        }
        if(difficulty == 0){
            first = random.nextInt(50)+1;
            second = random.nextInt(50)+1;
            this.andswer = first + second;
            s = String.valueOf(first)+"+"+String.valueOf(second)+"=";
        }
        if(difficulty == 1) {
            first = random.nextInt(10)+1;
            second = random.nextInt(10)+1;
            this.andswer = first * second;
            s = String.valueOf(first)+"x"+String.valueOf(second)+"=";
        }
        if(difficulty == 3) {
            int a1 = random.nextInt(10)+1;
            int a2 = random.nextInt(10)+1;
            first = a1*a2;
            int b1 = random.nextInt(a1)+1;
            int b2 = random.nextInt(a2)+1;
            second = b1*b2;
            this.andswer = first-second;
            s = String.valueOf(a1)+"x"+String.valueOf(a2)+"-"+String.valueOf(b1)+"x"+String.valueOf(b2)+"=";
        }
        expression.setText(s);
    }

    public void stop_player(View view){
        vibrator.cancel();
        player.stop();
        finish();
    }

    public void settings(View view){
        Intent i = new Intent();
        i.setClassName("com.orbita.alarm", "com.orbita.alarm.settings");
        startActivity(i);
    }

    @Override
    public void onBackPressed() {}

    @Override
    public void onDestroy(){
        super.onDestroy();
        vibrator.cancel();
        player.stop();
    }
}
