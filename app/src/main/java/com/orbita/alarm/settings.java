package com.orbita.alarm;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class settings extends AppCompatActivity implements FileBrowseDialog.MyDialogListener {

    ListView listView;
    LinearLayout listLay;
    RelativeLayout tabLay;
    TableLayout dayLay;
    RelativeLayout timeLay;
    NumberPicker n1;
    NumberPicker n2;
    Button save_back;
    Button ready;

    private ArrayAdapter<alarmItem> newAd;
    private List<alarmItem> alarmItemList;

    private boolean[] weekCheck = new boolean[7];
    private int myHour;
    private int myMinute;
    private Boolean on;
    private int freq = 1;
    private Boolean kind;
    private String musComposition;

    SharedPreferences sPref;
    alarmStorage storage;
    private alarmManager alarm;

    TextView buttonOpenDialog;
    Boolean fromMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        interfaceSetup();

        fromMain = getIntent().getBooleanExtra("fromMain", false);
        if(fromMain){
            SharedPreferences sr = getSharedPreferences("example", Context.MODE_PRIVATE);
            alarmStorage st2 = new alarmStorage(sr);

            LinearLayout l = (LinearLayout) findViewById(R.id.settLay);
            ready = (Button) findViewById(R.id.ready);
            Spinner sp = (Spinner) findViewById(R.id.difficlty_setter);

            sp.setSelection(st2.loadExample());
            Button s = (Button) findViewById(R.id.button4);
            s.setBackground(getResources().getDrawable(R.drawable.btn_back));

            listLay.setVisibility(View.GONE);
            tabLay.setVisibility(View.GONE);
            timeLay.setVisibility(View.GONE);
            dayLay.setVisibility(View.GONE);
            ready.setVisibility(View.GONE);
            l.setVisibility(View.VISIBLE);
        }
        else {
            LayoutInflater inflater = getLayoutInflater();
            final ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.item_type1, listView, false);
            listView.addFooterView(footer, null, false);

            footer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listLay.setVisibility(View.GONE);
                    timeLay.setVisibility(View.VISIBLE);
                }
            });

            sPref = getSharedPreferences("alarm", MODE_PRIVATE);
            storage = new alarmStorage(sPref);
            alarm = new alarmManager();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    currentAlarmEdit(position);
                    listLay.setVisibility(View.GONE);
                    timeLay.setVisibility(View.VISIBLE);
                    save_back.setVisibility(View.VISIBLE);
                    alarmItemList.remove(position);
                    newAd.notifyDataSetChanged();
                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> parent, View view,
                                               final int position, long id) {
                    AlertDialog.Builder ad = new AlertDialog.Builder(settings.this);
                    ad.setTitle(getResources().getString(R.string.Delete));
                    ad.setPositiveButton(getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alarmItemList.remove(position);
                            if (alarmItemList.size() > 0) {
                                newAd.notifyDataSetChanged();
                            }
                            newAd.notifyDataSetChanged();
                        }
                    });
                    ad.setNegativeButton(getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    ad.show();
                    return true;
                }
            });

            if (storage.alarmLoad().equals("alarm")) {
                alarmItemList = new ArrayList<alarmItem>();
                newAd = new AlarmItemArrayAdapter(this, alarmItemList);
                listView.setAdapter(newAd);
                newAd.notifyDataSetChanged();
            } else {
                alarmItemList = storage.alarmParse(getResources().getStringArray(R.array.days));
                newAd = new AlarmItemArrayAdapter(this, alarmItemList);
                listView.setAdapter(newAd);
                newAd.notifyDataSetChanged();
            }

            listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        }
        AD_setup();
    }

    protected void AD_setup(){

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9323060416361289~3356483292");
        AdView mAdView = (AdView) findViewById(R.id.adView);
//        mAdView.setAdSize(AdSize.SMART_BANNER);
//        mAdView.setAdUnitId("ca-app-pub-9323060416361289/4833216499");
//        mAdView.clearDisappearingChildren();
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onPause(){
        super.onPause();

//        if((alarmItemList.size() != 0) && (!toMain)) {
//            startRepeatingAlarm(alarmProcessing());
//        }

        if(!fromMain) {
            if (alarmItemList.isEmpty()) {
                storage.alarmClearing();
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
                cancelRepeatingTimer();
            } else {
                storage.alarmSave(alarmItemList);
            }
        }
    }

    //Инициализация режима редактирования
    protected void currentAlarmEdit(int position){
        alarmItem currAlarm = alarmItemList.get(position);
        currAlarm.setVisibleDays(getResources().getStringArray(R.array.days));
        n1.setValue(currAlarm.hour);
        n2.setValue(currAlarm.minute);
        this.myHour = currAlarm.hour;
        this.myMinute = currAlarm.minute;

        RadioButton r1 = (RadioButton) findViewById(R.id.radioKorm);
        RadioButton r2 = (RadioButton) findViewById(R.id.radioResh);
        this.kind = currAlarm.getKind();
        if(currAlarm.getKind()){
            r2.setChecked(true);
            r1.setChecked(true);
        }
        else{
            r1.setChecked(true);
            r2.setChecked(true);
        }

        CheckBox c = (CheckBox) findViewById(R.id.checkOn);
        this.on = currAlarm.getOn();
        if(currAlarm.getOn()) {
            c.setChecked(true);
        }
        else{
            c.setChecked(false);
        }

        TextView t1 = (TextView) findViewById(R.id.melody_holder);
        this.musComposition = currAlarm.getMusComposition();
        String s = currAlarm.getMusComposition().substring(currAlarm.getMusComposition().lastIndexOf("/")+1,
                currAlarm.getMusComposition().length());
        t1.setText(s);

        CheckBox c1 = (CheckBox) findViewById(R.id.checkBox);
        CheckBox c2 = (CheckBox) findViewById(R.id.checkBox2);
        CheckBox c3 = (CheckBox) findViewById(R.id.checkBox3);
        CheckBox c4 = (CheckBox) findViewById(R.id.checkBox4);
        CheckBox c5 = (CheckBox) findViewById(R.id.checkBox5);
        CheckBox c6 = (CheckBox) findViewById(R.id.checkBox6);
        CheckBox c7 = (CheckBox) findViewById(R.id.checkBox7);

        CheckBox[] checks = new CheckBox[]{c1,c2,c3,c4,c5,c6,c7};
        boolean[] week = currAlarm.getWeekCheck();
        this.weekCheck = currAlarm.getWeekCheck();
        for(int i = 0; i<7; i++){
            checks[i].setChecked(week[i]);
        }

        TextView daysofweek = (TextView) findViewById(R.id.daysofweek);
        daysofweek.setText(currAlarm.getWeekDays());

        RadioButton rb1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton rb2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton rb3 = (RadioButton) findViewById(R.id.radioButton3);
        TextView repeat = (TextView) findViewById(R.id.repeat);
        this.freq = currAlarm.getFreq();
        if(currAlarm.getFreq() == 1){
            rb2.setChecked(true);
            rb1.setChecked(true);
            repeat.setText(getResources().getString(R.string.Every5));
        }
        if(currAlarm.getFreq() == 2){
            rb1.setChecked(true);
            rb2.setChecked(true);
            repeat.setText(getResources().getString(R.string.Every10));
        }
        if(currAlarm.getFreq() == 3){
            rb2.setChecked(true);
            rb3.setChecked(true);
            repeat.setText(getResources().getString(R.string.Every15));
        }
    }

    public void save_back(View view){
        if(tabLay.getVisibility() == View.VISIBLE){
            freqSelect();
            tabLay.setVisibility(View.GONE);
            timeLay.setVisibility(View.VISIBLE);
        }
        if(dayLay.getVisibility() == View.VISIBLE){
            daySelect();
            dayLay.setVisibility(View.GONE);
            timeLay.setVisibility(View.VISIBLE);
        }
        if(timeLay.getVisibility() == View.VISIBLE){
            onSelect();
            timeSelect();
            fillItem(view);
            Sbros();
            newAd.notifyDataSetChanged();
            timeLay.setVisibility(View.GONE);
            listLay.setVisibility(View.VISIBLE);
        }
        save_back.setVisibility(View.GONE);
    }

    //Инициализация переменных элементами интерфейса
    protected void interfaceSetup(){
        this.listView = (ListView) findViewById(R.id.listView);
        this.listLay = (LinearLayout) findViewById(R.id.listLay);
        this.tabLay = (RelativeLayout) findViewById(R.id.tabLay);
        this.dayLay = (TableLayout) findViewById(R.id.dayLay);
        this.timeLay = (RelativeLayout) findViewById(R.id.timeLay);
        this.n1 = (NumberPicker) findViewById(R.id.numberPicker);
        this.n2 = (NumberPicker) findViewById(R.id.numberPicker2);
        this.save_back = (Button) findViewById(R.id.save_back);

        n1.setMinValue(0);
        n1.setMaxValue(23);
        n2.setMinValue(0);
        n2.setMaxValue(59);
        setDividerColor(n1, Color.argb(0,0,0,0));
        setDividerColor(n2, Color.argb(0,0,0,0));

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.click_one);
        mp.setVolume(0.35f, 0.35f);
        n1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener(){
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mp.start();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mp.pause();
                    }
                }, 500);
            }
        });

        n2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener(){
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mp.start();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mp.pause();
                    }
                }, 500);
            }
        });

        buttonOpenDialog = (TextView) findViewById(R.id.melody_holder);
        buttonOpenDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFBDialog();
            }
        });

        Button b = (Button) findViewById(R.id.ready);
        String t = getResources().getString(R.string.Ready);
        b.setTextSize(25);
        b.setText(t);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/EURO-DN_0.TTF");
        b.setTypeface(custom_font);
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            if (pf.getName().equals("Typeface")) {
                try {
                    Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/EURO-B.TTF");
                    pf.set(picker, custom_font);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    protected void Sbros(){
        CheckBox c = (CheckBox) findViewById(R.id.checkOn);
        RadioButton r1 = (RadioButton) findViewById(R.id.radioKorm);
        RadioButton r2 = (RadioButton) findViewById(R.id.radioResh);
        TextView t1 = (TextView) findViewById(R.id.melody_holder);

        c.setChecked(false);
        r1.setChecked(false);
        r2.setChecked(false);
        t1.setText(getResources().getString(R.string.melody_name));

        n1.setValue(0);
        n2.setValue(0);

        CheckBox c1 = (CheckBox) findViewById(R.id.checkBox);
        CheckBox c2 = (CheckBox) findViewById(R.id.checkBox2);
        CheckBox c3 = (CheckBox) findViewById(R.id.checkBox3);
        CheckBox c4 = (CheckBox) findViewById(R.id.checkBox4);
        CheckBox c5 = (CheckBox) findViewById(R.id.checkBox5);
        CheckBox c6 = (CheckBox) findViewById(R.id.checkBox6);
        CheckBox c7 = (CheckBox) findViewById(R.id.checkBox7);
        CheckBox[] checks = new CheckBox[]{c1,c2,c3,c4,c5,c6,c7};

        for(int i=0; i<7; i++){
            checks[i].setChecked(false);
        }
        TextView daysofweek = (TextView) findViewById(R.id.daysofweek);
        TextView repeat = (TextView) findViewById(R.id.repeat);
        daysofweek.setText(getResources().getString(R.string.Today));
        repeat.setText(getResources().getString(R.string.Every5));

        RadioButton r10 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton r20 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton r30 = (RadioButton) findViewById(R.id.radioButton3);

        r10.setChecked(true);
        r20.setChecked(false);
        r30.setChecked(false);

        this.freq = 1;
        this.weekCheck = new boolean[]{false,false,false,false,false,false,false};
        this.musComposition = getResources().getString(R.string.melody_name);
    }

    //Переход между layout-ами
    public void jump(View view){
        for(int i=0; i<1; i++) {
            if (timeLay.getVisibility() == View.VISIBLE) {
                onSelect();
                timeSelect();
                fillItem(view);
                Sbros();
                timeLay.setVisibility(View.GONE);
                listLay.setVisibility(View.VISIBLE);
                save_back.setVisibility(View.GONE);
                break;
            }
            if (dayLay.getVisibility() == View.VISIBLE) {
                daySelect();
                dayLay.setVisibility(View.GONE);
                timeLay.setVisibility(View.VISIBLE);
                break;
            }
            if (tabLay.getVisibility() == View.VISIBLE) {
                freqSelect();
                tabLay.setVisibility(View.GONE);
                timeLay.setVisibility(View.VISIBLE);
                break;
            }
            if(listLay.getVisibility() == View.VISIBLE){
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("fromSettings", true);
                startActivity(intent);
                break;
            }
        }
    }

    public void weekJump(View view){
        timeLay.setVisibility(View.GONE);
        dayLay.setVisibility(View.VISIBLE);
    }

    public void freqJump(View view){
        timeLay.setVisibility(View.GONE);
        tabLay.setVisibility(View.VISIBLE);
    }

    //Заполняем элемент списка
    protected void fillItem(View view){

        alarmItem alarm = new alarmItem();

        alarm.setVisibleDays(getResources().getStringArray(R.array.days));
        alarm.setWeekCheck(this.weekCheck);
        alarm.setTime(this.myHour, this.myMinute);
        alarm.setOn(this.on);
        alarm.setKind(this.kind);
        alarm.setFreq(this.freq);
        alarm.setMusComposition(this.musComposition);

        alarmItemList.add(alarm);

        newAd.notifyDataSetChanged();
    }

    //Выбираем время будильника
    protected void timeSelect(){
        int a= n1.getValue();
        int b = n2.getValue();
        myHour = a;
        myMinute = b;
    }

    //Включаем будильник и задаём его тип
    protected void onSelect(){
        CheckBox c = (CheckBox) findViewById(R.id.checkOn);
        RadioButton r1 = (RadioButton) findViewById(R.id.radioKorm);
//        RadioButton r2 = (RadioButton) findViewById(R.id.radioResh);
        boolean local = c.isChecked();
        if(local){
            this.on = true;
        }
        else{
            this.on = false;
        }

        if(r1.isChecked()){
            kind = true;
        }
        else{
            kind = false;
        }

        if(this.musComposition == null){
            this.musComposition = getResources().getString(R.string.melody_name);
        }
    }

    //Задаём дату
    protected void daySelect(){

        CheckBox c1 = (CheckBox) findViewById(R.id.checkBox);
        CheckBox c2 = (CheckBox) findViewById(R.id.checkBox2);
        CheckBox c3 = (CheckBox) findViewById(R.id.checkBox3);
        CheckBox c4 = (CheckBox) findViewById(R.id.checkBox4);
        CheckBox c5 = (CheckBox) findViewById(R.id.checkBox5);
        CheckBox c6 = (CheckBox) findViewById(R.id.checkBox6);
        CheckBox c7 = (CheckBox) findViewById(R.id.checkBox7);

        CheckBox[] checks = new CheckBox[]{c1,c2,c3,c4,c5,c6,c7};
        String[] days = getResources().getStringArray(R.array.days);
        String weekDays = "";
        boolean[] local = new boolean[7];

        for(int i=0; i<7; i++){
            local[i] = checks[i].isChecked();
            if(checks[i].isChecked()){
                weekDays += days[i];
            }
        }
        this.weekCheck = local;

        TextView daysofweek = (TextView) findViewById(R.id.daysofweek);
        daysofweek.setText(weekDays);
    }

    //Задаём частоту повторений
    protected void freqSelect(){
        RadioButton r1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton r2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton r3 = (RadioButton) findViewById(R.id.radioButton3);
        TextView repeat = (TextView) findViewById(R.id.repeat);
        freq = 1;
        if(r1.isChecked()){
            freq = 1;
            repeat.setText(getResources().getString(R.string.Every5));
        }
        if(r2.isChecked()){
            freq = 2;
            repeat.setText(getResources().getString(R.string.Every10));
        }
        if(r3.isChecked()){
            freq = 3;
            repeat.setText(getResources().getString(R.string.Every15));
        }
    }

    //Переключение в меню доп. настроек
    public void showSettings(View view){
        SharedPreferences sr = getSharedPreferences("example", Context.MODE_PRIVATE);
        alarmStorage st2 = new alarmStorage(sr);

        LinearLayout l = (LinearLayout) findViewById(R.id.settLay);
//        AdView mAdView = (AdView) findViewById(R.id.adView);
        ready = (Button) findViewById(R.id.ready);
        Spinner sp = (Spinner) findViewById(R.id.difficlty_setter);

        if(l.getVisibility() == View.GONE){
            sp.setSelection(st2.loadExample());

            Button ss = (Button) findViewById(R.id.button4);
            ss.setBackground(getResources().getDrawable(R.drawable.btn_back));

            listLay.setVisibility(View.GONE);
            tabLay.setVisibility(View.GONE);
            timeLay.setVisibility(View.GONE);
            dayLay.setVisibility(View.GONE);
//            mAdView.setVisibility(View.GONE);
            ready.setVisibility(View.GONE);
            l.setVisibility(View.VISIBLE);
        }
        else{
            int s = sp.getSelectedItemPosition();

            st2.exampleSave(sp.getSelectedItemPosition());

            if(fromMain){
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("fromSettings", true);
                startActivity(intent);
            }
            else{
                l.setVisibility(View.GONE);
                Button ss = (Button) findViewById(R.id.button4);
                ss.setBackground(getResources().getDrawable(R.drawable.btn_settings));
//                mAdView.setVisibility(View.VISIBLE);
                ready.setVisibility(View.VISIBLE);
                listLay.setVisibility(View.VISIBLE);
            }
        }

    }

    public void playMarket(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Design%20Orbita&hl=ru"));
        startActivity(intent);
    }

    public void RateApp(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.orbita.alarm"));
        startActivity(intent);
    }

    public void RemoveAds(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.orbita.alarmpro"));
        startActivity(intent);
    }

    public void showFBDialog(){
        FileBrowseDialog fb = new FileBrowseDialog();
        fb.show(getSupportFragmentManager(),
                "fb_dialog");
    }

    @Override
    public void onDialogListItemClick(DialogFragment dialog, String mComp){
        this.musComposition = mComp;
    }

    public void cancelRepeatingTimer() {
        Context context = this.getApplicationContext();
        if (alarm != null) {
            alarm.CancelAlarm(context);
            Log.d("Error", "Alarm deleted!");
        } else {
            Log.d("Error", "Alarm is not deleted!");
        }
    }

    @Override
    public void onBackPressed() {}
}
