package com.orbita.alarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RelativeLayout l1;
    RelativeLayout l2;
    TextView day_and_date;
    String[] arr;

    private List<alarmItem> alarmItemList;
    SharedPreferences sPref;
    private alarmStorage storage;
    private alarmManager alarm;
    private NotificationManager mNotificationManager;

    private boolean fromSettings;
    private boolean fromAlarm;
    private boolean toSettings = false;

    InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AD_setup();

        arr = new String[]{getResources().getString(R.string.Sunday),
                getResources().getString(R.string.Monday),getResources().getString(R.string.Tuesday),
                getResources().getString(R.string.Wednesday),getResources().getString(R.string.Thursday),
                getResources().getString(R.string.Friday),getResources().getString(R.string.Saturday)};
        String[] mons = new String[]{getResources().getString(R.string.January),
                getResources().getString(R.string.February),getResources().getString(R.string.March),
                getResources().getString(R.string.April),getResources().getString(R.string.May),
                getResources().getString(R.string.June),getResources().getString(R.string.July),
                getResources().getString(R.string.August),getResources().getString(R.string.September),
                getResources().getString(R.string.October),getResources().getString(R.string.November),
                getResources().getString(R.string.December)};
        Calendar current = Calendar.getInstance();
        day_and_date = (TextView) findViewById(R.id.day_and_date);

        String currday = arr[current.get(Calendar.DAY_OF_WEEK)-1];
        String currmon = mons[current.get(Calendar.MONTH)];
        String all = currday+" "+String.valueOf(current.get(Calendar.DAY_OF_MONTH))+" "+currmon;
        day_and_date.setText(all);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        fromSettings = getIntent().getBooleanExtra("fromSettings", false);
        fromAlarm = getIntent().getBooleanExtra("fromAlarm", false);

        initText();

        if(fromAlarm){
            StringBuilder msgStr = new StringBuilder();
            Format formatter = new SimpleDateFormat("HH:mm");
            msgStr.append(formatter.format(new Date()));
            TextView t1 = (TextView) findViewById(R.id.textView19);
            t1.setText(msgStr);

            l1 = (RelativeLayout) findViewById(R.id.l1);
            l2 = (RelativeLayout) findViewById(R.id.l2);
            l1.setVisibility(View.GONE);
            l2.setVisibility(View.VISIBLE);
            mNotificationManager.cancelAll();
        }

        //Calendar current = Calendar.getInstance();

        if((fromAlarm && !toSettings) || (fromSettings && !toSettings)){
            sPref = getSharedPreferences("alarm", MODE_PRIVATE);
            storage = new alarmStorage(sPref);
            alarm = new alarmManager();

            if(!storage.alarmLoad().equals("alarm")){
                alarmItemList = storage.alarmParse(getResources().getStringArray(R.array.days));

                ArrayList<alarmItem> enabled_alarms = new ArrayList<alarmItem>();
                for(int i =0; i <alarmItemList.size(); i++){
                    if(alarmItemList.get(i).getOn()){
                        enabled_alarms.add(alarmItemList.get(i));
                    }
                }

                if(enabled_alarms.size()>0) {
                    Calendar cal = alarmProcessing(enabled_alarms);


                    String day;
                    day = arr[cal.get(Calendar.DAY_OF_WEEK) - 1];

                    if (cal.get(Calendar.DAY_OF_YEAR) == current.get(Calendar.DAY_OF_YEAR)) {
                        day = getResources().getString(R.string.Today);
                    }

                    if (cal.get(Calendar.DAY_OF_YEAR) == current.get(Calendar.DAY_OF_YEAR) + 1) {
                        day = getResources().getString(R.string.Tomorrow);
                    }

                    String h = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
                    if (h.equals("0")) {
                        h = "00";
                    }

                    String m = String.valueOf(cal.get(Calendar.MINUTE));
                    if (cal.get(Calendar.MINUTE) < 10) {
                        m = "0" + m;
                    }
                    if (m.equals("0")) {
                        m = "00";
                    }

                    String ss = h + ":" + m;

                    Context context = getApplicationContext();
                    Resources res = context.getResources();
                    NotificationCompat.Builder mBuilder =
                            (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.drawable.alarm_uvedomlenie_2)
                                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.alarm_uvedomlenie_2))
                                    .setContentTitle(getResources().getString(R.string.Alarm))
                                    .setContentText(day + " " + ss)
                                    .setOngoing(true);
                    Intent resultIntent = new Intent(this, settings.class);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addParentStack(settings.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent =
                            stackBuilder.getPendingIntent(
                                    0,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    mBuilder.setContentIntent(resultPendingIntent);

                    mNotificationManager.notify(1, mBuilder.build());

                    startRepeatingAlarm(cal);
                }
                else{
                    mNotificationManager.cancelAll();
                    cancelRepeatingTimer();
                }
            }
            else{
                alarmItemList = new ArrayList<alarmItem>();
                mNotificationManager.cancelAll();
            }
        }
    }

    protected void AD_setup(){
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-9323060416361289/6309949692");
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("5F7E4BB97D33ED00401836180A0C7BBB")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    protected void initText(){
        TextView t1 = (TextView) findViewById(R.id.textView20);
        TextView t2 = (TextView) findViewById(R.id.textView22);
        TextView t3 = (TextView) findViewById(R.id.textView21);

        String s1 = getResources().getString(R.string.Start);
        String s2 = getResources().getString(R.string.Customize);
        String s3 = getResources().getString(R.string.GoodMorning);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/EURO-B.TTF");

        t1.setTextSize(28);
        t1.setTextColor(Color.parseColor("#6aa134"));
        t1.setText(s1);
        t1.setTypeface(custom_font);

        t2.setTextSize(25);
        t2.setTextColor(Color.parseColor("#6aa134"));
        t2.setText(s2);
        t2.setTypeface(custom_font);

        t3.setTextSize(25);
        t3.setTextColor(Color.parseColor("#84f019"));
        t3.setText(s3);
        t3.setTypeface(custom_font);
    }

    public void openSettings(View view) {
        this.toSettings = true;
        Intent intent = new Intent(this, settings.class);
        startActivity(intent);
    }

    public void openAdditional(View view){
        this.toSettings = true;
        Intent intent = new Intent(this, settings.class);
        intent.putExtra("fromMain", true);
        startActivity(intent);
    }

    public void goodMorning(View view){
        l2.setVisibility(View.GONE);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        l1.setVisibility(View.VISIBLE);
    }

    public Calendar alarmProcessing(ArrayList<alarmItem> enabled_alarms){

        Calendar[] dates = new Calendar[enabled_alarms.size()];

        for(int i = 0; i<enabled_alarms.size(); i++){
            int day = enabled_alarms.get(i).getNextWeekDay();
            dates[i] = Calendar.getInstance();

            if(day == dates[i].get(Calendar.DAY_OF_WEEK)){
                dates[i].add(Calendar.DAY_OF_YEAR,0);
                if(enabled_alarms.get(i).hour < dates[i].get(Calendar.HOUR_OF_DAY)){
                    dates[i].add(Calendar.DAY_OF_YEAR,7);
                }
                if(enabled_alarms.get(i).hour == dates[i].get(Calendar.HOUR_OF_DAY)) {
                    if (enabled_alarms.get(i).minute <= dates[i].get(Calendar.MINUTE)) {
                        dates[i].add(Calendar.DAY_OF_YEAR,7);
                    }
                }
            }
            if(day > dates[i].get(Calendar.DAY_OF_WEEK)){
                dates[i].add(Calendar.DAY_OF_YEAR,day-dates[i].get(Calendar.DAY_OF_WEEK));
            }
            if(day < dates[i].get(Calendar.DAY_OF_WEEK)){
                dates[i].add(Calendar.DAY_OF_YEAR,(7-dates[i].get(Calendar.DAY_OF_WEEK))+day);
            }
        }

        for(int i = 0; i<enabled_alarms.size(); i++) {
            dates[i].set(Calendar.HOUR, enabled_alarms.get(i).hour);
            dates[i].set(Calendar.HOUR_OF_DAY, enabled_alarms.get(i).hour);
            dates[i].set(Calendar.MINUTE, enabled_alarms.get(i).minute);
            dates[i].set(Calendar.SECOND, 1);
        }

        long[] difference = new long[enabled_alarms.size()];
        long element;
        int index = 0;

        if(enabled_alarms.size()>1) {

            for (int i = 0; i < enabled_alarms.size(); i++) {
                difference[i] = dates[i].getTimeInMillis() - System.currentTimeMillis();
            }

            element = difference[0];

            for (int i = 0; i < enabled_alarms.size(); i++) {
                if ((difference[i] < element) && (difference[i] > 0)) {
                    element = difference[i];
                    index = i;
                }
            }
        }

        alarm.setFreq(alarmItemList.get(index).freq);

        SharedPreferences sr = getSharedPreferences("kind", Context.MODE_PRIVATE);
        alarmStorage st2 = new alarmStorage(sr);
        st2.kindSave(alarmItemList.get(index).kind);

        SharedPreferences sr1 = getSharedPreferences("music", Context.MODE_PRIVATE);
        st2.setPreferences(sr1);
        st2.musicSave(alarmItemList.get(index).musComposition);
//        alarmStorage st1 =  new alarmStorage(sr1);
//        st1.musicSave(alarmItemList.get(index).musComposition);

        return dates[index];
    }

    protected void startRepeatingAlarm(Calendar c){
        Context context = this.getApplicationContext();
        if(alarm != null){
            alarm.SetAlarm(context, c);
        }else{
            Log.d("Error", "Alarm not initialised!");
        }
    }

    public void cancelRepeatingTimer() {
        Context context = this.getApplicationContext();
        if (alarm != null) {
            alarm.CancelAlarm(context);
            Log.d("Error", "Alarm deleted!");
        } else {
            Log.d("Error", "Alarm is not deleted!");
        }
    }
}
