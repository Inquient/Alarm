package com.orbita.alarm;

/**
 * Created by Админ on 11.06.2016.
 */

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class AlarmItemArrayAdapter extends ArrayAdapter<alarmItem>{

    private final Activity context;
    private final List<alarmItem> list;

    public AlarmItemArrayAdapter(Activity context, List<alarmItem> list){
        super(context, R.layout.item_type2, list);
        this.context = context;
        this.list = list;
    }

    private static class ViewHolder {
        protected TextView text;
        protected TextView text2;
        protected CheckBox checkbox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = null;

        if(convertView == null)  {
            LayoutInflater inflater=context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.item_type2, parent, false);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.label);
            viewHolder.text2 = (TextView) rowView.findViewById(R.id.label2);
            viewHolder.checkbox  = (CheckBox) rowView.findViewById(R.id.check);

            viewHolder.checkbox.setOnCheckedChangeListener(myCheckChangList);
            viewHolder.checkbox.setTag(position);
            rowView.setTag(viewHolder);
        }

        else {
            rowView = convertView;
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.text.setText(list.get(position).getTime());
        holder.text2.setText(list.get(position).getWeekDays());
        holder.checkbox.setChecked(list.get(position).getOn());

        return rowView;

    };

    private alarmItem getAlarm(int position) {
        return ((alarmItem) getItem(position));
    }

    private CompoundButton.OnCheckedChangeListener myCheckChangList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
//            getAlarm((Integer) buttonView.getTag()).setOn(isChecked);
            list.get((Integer) buttonView.getTag()).setOn(isChecked);
        }
    };
}
